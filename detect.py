#! /usr/bin/env python3

import rospy
import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from std_msgs.msg import Int32
import time

lower_blue = np.array([109, 78, 10])
upper_blue = np.array([132, 95, 20])

lower_red = np.array([32,32,128])
upper_red = np.array([54,58,230])


class CameraNode:
    def __init__(self):
        # Creates a node called and registers it to the ROS master
        rospy.init_node('detect')

        # CvBridge is used to convert ROS messages to matrices manipulable by OpenCV
        self.bridge = CvBridge()

        # Initialize the node parameters
        self.last_detected_time = 0
        self.countdown_time_seconds = 4
        self.obstacleDetected = False  # Flag pour la détection des murs

        # Publisher to the output topics.
        self.pub_img = rospy.Publisher('~output', Image, queue_size=1)

        # Subscribers to the input topics.
        self.subscriber_img = rospy.Subscriber('/camera/image_rect_color', Image, self.callbackImage)
        self.subscriber_ultrasonic = rospy.Subscriber('/ultrasound', Int32, self.callbackUltrasonic)
        rospy.loginfo("Finished initialization")

    def callbackImage(self, msg):
        '''
        Function called when an image is received.
        msg: Image message received
        img_bgr: Width*Height*3 Numpy matrix storing the image
        '''
        rospy.loginfo("in image callback")
        print("image detected")
        if (self.obstacleDetected == False):
            rospy.loginfo("No obstacle detected")
            return

        # Convert ROS Image -> OpenCV
        try:
            img_bgr = self.bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError as e:
            rospy.logwarn(e)
            return

        blue_mask = cv2.inRange(img_bgr, lower_blue, upper_blue)
        red_mask = cv2.inRange(img_bgr, lower_red, upper_red)

        # Find contours in the masks
        red_contours, _ = cv2.findContours(red_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        blue_contours, _ = cv2.findContours(blue_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # Calculate the largest contour areas
        red_area = max([cv2.contourArea(cnt) for cnt in red_contours], default=0)
        blue_area = max([cv2.contourArea(cnt) for cnt in blue_contours], default=0)

        # Determine the direction based on the areas
        direction = None
        if red_area > blue_area and red_area > 0:
            direction = "à droite !"
        elif blue_area > red_area and blue_area > 0:
            direction = "à gauche !"
        print(direction)
        # State machine for detection
        #current_time = time.time()
        #if direction and (current_time - self.last_detected_time > self.countdown_time_seconds):
        #    rospy.loginfo(f"Direction: {direction}")
        #    self.last_detected_time = current_time

        # Optionally draw contours for debugging
        cv2.drawContours(img_bgr, red_contours, -1, (0, 0, 255), 2)
        cv2.drawContours(img_bgr, blue_contours, -1, (255, 0, 0), 2)

        # Convert OpenCV -> ROS Image and publish
        try:
            self.pub_img.publish(self.bridge.cv2_to_imgmsg(img_bgr, "bgr8"))
        except CvBridgeError as e:
            rospy.logwarn(e)
            
            
    # Callback for the ultrasonic sensor topic
    def callbackUltrasonic(self, msg):
        distance = msg.data
        #rospy.loginfo(f"Ultrasonic sensor reading: {distance}")

        if distance < 1500:
            self.obstacleDetected = True
            rospy.loginfo("Obstacle detected")
        else:
            self.obstacleDetected = False
            rospy.loginfo("No obstacle detected")

    

if __name__ == '__main__':
    # Start the node and wait until it receives a message or stopped by Ctrl+C
    node = CameraNode()
    rospy.spin()
