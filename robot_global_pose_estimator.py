#!/usr/bin/env python3

import numpy as np

import rospy
from geometry_msgs.msg import PoseStamped
from turtlebot3_msgs.msg import SensorState
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import PointCloud, Imu
from geometry_msgs.msg import Point
from std_msgs.msg import Float32, String, Int32

from tf.transformations import quaternion_from_euler


def coordinates_to_message(x, y, O, t):
    msg = PoseStamped()
    msg.pose.position.x = x
    msg.pose.position.y = y
    [msg.pose.orientation.x,
    msg.pose.orientation.y,
    msg.pose.orientation.z,
    msg.pose.orientation.w] = quaternion_from_euler(0.0, 0.0, O)
    msg.header.stamp = t
    msg.header.frame_id = 'odom'
    return msg


class Odom2PoseNode:
    def __init__(self):
        rospy.init_node('odom2pose')

        # Constants
        self.ENCODER_RESOLUTION = 4096
        self.WHEEL_RADIUS = 0.033
        self.WHEEL_SEPARATION = 0.160
        self.MAG_OFFSET = np.pi / 2.0 - 0.07

        # Variables
        self.x_odom, self.y_odom, self.O_odom = 0, 0, 0
        self.dx_odom, self.dy_odom = 0, 0
        self.x_gyro, self.y_gyro, self.O_gyro = 0, 0, 0
        self.x_magn, self.y_magn, self.O_magn = 0, 0, 0
        self.x_lidar, self.y_lidar = 0, 0
        self.x_global, self.y_global = 0, 0
        self.prev_obs_front, self.prev_obs_left, self.prev_obs_right = 0, 0, 0
        self.O_global = 0
        self.prev_left_encoder = 0
        self.prev_right_encoder = 0
        self.prev_enco_t = 0
        self.prev_gyro_t = 0
        self.centering_ratio = 1
        self.v = 0
        self.W_THRESHOLD = 0.05

        self.all_points = []
        self.all_ultrasound_points = []
        self.ransac_points = []
        

        # Publishers
        self.pub_enco = rospy.Publisher('/pose_enco', PoseStamped, queue_size=10)
        self.pub_pose_globale = rospy.Publisher('/pose_globale', PoseStamped, queue_size=10)
        self.pub_lidar = rospy.Publisher('/scan_global', PointCloud, queue_size=10)
        self.pub_obs_rear = rospy.Publisher('/surrounding/obs_rear', Float32, queue_size=10)
        self.pub_obs_front = rospy.Publisher('/surrounding/obs_front', Float32, queue_size=10)
        self.pub_obs_left = rospy.Publisher('/surrounding/obs_left', Float32, queue_size=10)
        self.pub_obs_right = rospy.Publisher('/surrounding/obs_right', Float32, queue_size=10)
        self.pub_direction = rospy.Publisher('/direction_to_take', String, queue_size=10)
        self.pub_filtered = rospy.Publisher('/scan_median_filtered', PointCloud, queue_size=10)
        # self.pub_ultrasound = rospy.Publisher('/surrounding/ultrasound', PointCloud, queue_size=10)

        # Subscribers
        self.sub_enco = rospy.Subscriber('/sensor_state', SensorState, self.callback_enco)
        self.sub_lidar = rospy.Subscriber('/scan', LaserScan, self.callback_lidar)
        # self.sub_ultrasound = rospy.Subscriber('/ultrasound', Int32, self.callback_ultrasound)
        self.sub_imu = rospy.Subscriber('/imu', Imu, self.callback_imu)



    def callback_enco(self, sensor_state):
        """
        Callback function for the encoder subscriber
        The result is a message containing the current pose of the robot in the global frame
        Global frame is dependent on the starting position of the robot
        """

        # Compute the differential in encoder count
        dt = sensor_state.header.stamp.to_sec() - self.prev_enco_t
        dq_left = sensor_state.left_encoder - self.prev_left_encoder
        dq_right = sensor_state.right_encoder - self.prev_right_encoder

        first = self.prev_enco_t == 0
        self.prev_enco_t = sensor_state.header.stamp.to_sec()
        self.prev_left_encoder = sensor_state.left_encoder
        self.prev_right_encoder = sensor_state.right_encoder
        if first: return
        # Compute the linear and angular velocity (self.v and w)
        self.v = (dq_left + dq_right) / 2.0 * 2 * np.pi * self.WHEEL_RADIUS / self.ENCODER_RESOLUTION / dt
        self.w = (dq_right - dq_left) * 2 * np.pi * self.WHEEL_RADIUS / self.WHEEL_SEPARATION / self.ENCODER_RESOLUTION / dt

        # Update x_odom, y_odom and O_odom accordingly
        self.x_odom += self.v * np.cos(self.O_odom) * dt
        self.y_odom += self.v * np.sin(self.O_odom) * dt
        self.O_odom += self.w * dt
        
        self.dx_odom = self.v * np.cos(self.O_odom) * dt
        self.dy_odom = self.v * np.sin(self.O_odom) * dt
        
        if self.prev_obs_front == 0 or abs(self.w) > self.W_THRESHOLD or self.centering_ratio < 0.5 or self.centering_ratio > 1.5:
            self.x_global += self.dx_odom
            self.y_global += self.dy_odom
            self.x_lidar += self.dx_odom
            self.y_lidar += self.dy_odom

        msg = coordinates_to_message(self.x_odom, self.y_odom, self.O_odom, sensor_state.header.stamp)
        self.pub_enco.publish(msg)
        
    def callback_imu(self, imu):
        """
        Callback function for the IMU subscriber
        The result is a attribute containing a complementary fusion of the theta of the robot
        """
        # Compute the complementary filter
        dt = imu.header.stamp.to_sec() - self.prev_gyro_t
        self.prev_gyro_t = imu.header.stamp.to_sec()
        self.O_gyro += imu.angular_velocity.z * dt
        self.O_magn = imu.orientation.z + self.MAG_OFFSET
        self.O_global = 0.65 * self.O_odom + 0.35 * self.O_gyro
        
    # def callback_ultrasound(self, ultrasound):
    #     """
    #     Callback function for the ultrasound subscriber
    #     """
    #     T_global = np.array([[np.cos(self.O_global), -np.sin(self.O_global), self.x_odom],
    #                             [np.sin(self.O_global), np.cos(self.O_global), self.y_odom],
    #                             [0, 0, 1]])
    #     us_point = np.array([[ultrasound.data/10000], [0], [1]])
    #     us_global = np.dot(T_global, us_point)
    #     self.all_ultrasound_points.append(Point(x=us_global[0], y=us_global[1]))
    #     msg = PointCloud()
    #     msg.header.stamp = rospy.Time.now()
    #     msg.header.frame_id = 'odom'
    #     msg.points = self.all_ultrasound_points
    #     self.pub_ultrasound.publish(msg)
    #     self.pub_obs_front.publish(ultrasound.data/10000)
        

    def callback_lidar(self, scan):
        """
        Callback function for the lidar subscriber
        The result is a message containing the current scan of the robot in the global frame
        Global frame is dependent on the starting position of the robot
        """
        # Transformation Matrix from the robot frame to the global frame
        T = np.array([[np.cos(self.O_global), -np.sin(self.O_global), self.x_global],
                      [np.sin(self.O_global), np.cos(self.O_global), self.y_global],
                      [0, 0, 1]])
        # Transformation Matrix from the Lidar frame to the robot frame
        lidar_angular_offset = 0
        T_lidar = np.array([[np.cos(lidar_angular_offset), -np.sin(lidar_angular_offset), 0],
                            [np.sin(lidar_angular_offset), np.cos(lidar_angular_offset), 0],
                            [0, 0, 1]])
        # Position matrixes of the lidar points
        point_global_frame = np.zeros((2, len(scan.ranges)))

        # We want to identify the closest obstacle in each direction based on a cone of 20 degrees
        # /scan provides angles in rad
        cone = 5 * np.pi / 180
        # Front obstacle
        obs_front = 1e31 # distance to the closest obstacle in front, initialized to a large number
        for i in range(len(scan.ranges)):
            if scan.ranges[i] == float('inf') or scan.ranges[i] < 0.01:
                continue
            if scan.angle_min + i * scan.angle_increment < cone:
                obs_front = min(obs_front, scan.ranges[i]) # Sens positif
            if scan.angle_min + i * scan.angle_increment > 2 * np.pi - cone:
                obs_front = min(obs_front, scan.ranges[i])
        self.pub_obs_front.publish(obs_front)
        # Left obstacle
        obs_left = 1e31
        for i in range(len(scan.ranges)):
            if scan.ranges[i] == float('inf') or scan.ranges[i] < 0.01:
                continue
            if scan.angle_min + i * scan.angle_increment > np.pi / 2 - cone and scan.angle_min + i * scan.angle_increment < np.pi / 2 + cone:
                obs_left = min(obs_left, scan.ranges[i])
        self.pub_obs_left.publish(obs_left)
        # Right obstacle
        obs_right = 1e31
        for i in range(len(scan.ranges)):
            if scan.ranges[i] == float('inf') or scan.ranges[i] < 0.01:
                continue
            if scan.angle_min + i * scan.angle_increment > 3*np.pi / 2 - cone and scan.angle_min + i * scan.angle_increment < 3*np.pi / 2 + cone:
                obs_right = min(obs_right, scan.ranges[i])
        self.pub_obs_right.publish(obs_right)

        # With these information, we can compute a ratio between the left and right obstacles. This ratio will be used to determine the direction of the robot
        self.centering_ratio = obs_left / obs_right
        # When the ratio is close to 1, the robot is centered
        # When the ratio is greater than 1, the robot is closer to the left
        # When the ratio is smaller than 1, the robot is closer to the right

        if self.centering_ratio < 0.6:
            self.pub_direction.publish(f'Road on Right {self.centering_ratio}')
        elif self.centering_ratio > 1.4:
            self.pub_direction.publish(f'Road on Left {self.centering_ratio}')
        elif self.centering_ratio < 0.95:
            self.pub_direction.publish(f'Turn Light Right {self.centering_ratio}')
        elif self.centering_ratio > 1.05:
            self.pub_direction.publish(f'Turn Light Left {self.centering_ratio}')
            
        if self.prev_obs_front == 0 or abs(self.w) > self.W_THRESHOLD or self.centering_ratio < 0.5 or self.centering_ratio > 1.5:
            self.prev_obs_front = obs_front
            self.prev_obs_left = obs_left
            self.prev_obs_right = obs_right
            #.x_global += self.dx_odom
            #self.y_global += self.dy_odom
            self.pub_pose_globale.publish(coordinates_to_message(self.x_global, self.y_global, self.O_global, rospy.Time.now()))
            return
    
        for i in range(len(scan.ranges)):
            if scan.ranges[i] == float('inf'):
                continue
            x = scan.ranges[i] * np.cos((scan.angle_min + i * scan.angle_increment))
            y = scan.ranges[i] * np.sin((scan.angle_min + i * scan.angle_increment))
            # Transformation
            point_lidar_frame = np.array([[x], [y], [1]])
            point_robot_frame = np.dot(T_lidar, point_lidar_frame)
            point_global_frame[:, i] = np.dot(T, point_robot_frame)[:2, 0]
        
         # Publish the global frame scan
        msg = PointCloud()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = 'odom'
        for i in range(len(scan.ranges)):
            if scan.ranges[i] == float('inf') or scan.ranges[i] < 0.1:
                continue
            point = Point()
            point.x = point_global_frame[0, i]
            point.y = point_global_frame[1, i]
            self.all_points.append(point)
        msg.points = self.all_points
        self.pub_lidar.publish(msg)
    
        # Odometry from Lidar and obstacle detection
        delta_front = -(obs_front - self.prev_obs_front)
        # delta y = average of the left and right obstacles changes
        delta_side = -((obs_left - self.prev_obs_left + obs_right - self.prev_obs_right) / 2)
        # we dont compute theta, impossible with lidar without advanced algorithms
        
        # computing the x and y lidar
        # no angle transformation needed, we only get the linear movement when it's not turning
        self.x_lidar += delta_front * np.cos(self.O_global) + delta_side * np.sin(self.O_global)
        self.y_lidar += delta_side * np.cos(self.O_global) + delta_front * np.sin(self.O_global)
        
        #print(f'x_lidar: {self.x_lidar}, y_lidar: {self.y_lidar}')
               
        self.x_global = 0.7 * self.x_odom + 0.3 * self.x_lidar
        self.y_global = 0.7 * self.y_odom + 0.3 * self.y_lidar
        
        self.pub_pose_globale.publish(coordinates_to_message(self.x_global, self.y_global, self.O_odom, rospy.Time.now()))
        self.prev_obs_front = obs_front
        self.prev_obs_left = obs_left
        self.prev_obs_right = obs_right 



if __name__ == '__main__':
    node = Odom2PoseNode()
    try:
        rospy.spin()
    except rospy.ROSInterruptException:
        pass